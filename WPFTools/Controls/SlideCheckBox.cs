﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace WPFTools.Controls
{
    /// <summary>
    /// 类似移动端 选择框
    /// </summary>
    public class SlideCheckBox : CheckBox
    {
        static SlideCheckBox()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(SlideCheckBox), new FrameworkPropertyMetadata(typeof(SlideCheckBox)));
        }

      

        public static readonly DependencyProperty TextProperty = DependencyProperty.Register(
           "Text", typeof(string), typeof(SlideCheckBox), new PropertyMetadata("Off"));
        /// <summary>
        /// 默认文本（未选中）
        /// </summary>
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly DependencyProperty CheckedTextProperty = DependencyProperty.Register(
            "CheckedText", typeof(string), typeof(SlideCheckBox), new PropertyMetadata("On"));
        /// <summary>
        /// 选中状态文本
        /// </summary>
        public string CheckedText
        {
            get { return (string)GetValue(CheckedTextProperty); }
            set { SetValue(CheckedTextProperty, value); }
        }

        public static readonly DependencyProperty CheckedForegroundProperty =
            DependencyProperty.Register("CheckedForeground", typeof(Brush), typeof(SlideCheckBox), new PropertyMetadata(Brushes.WhiteSmoke));
        /// <summary>
        /// 选中状态前景样式
        /// </summary>
        public Brush CheckedForeground
        {
            get { return (Brush)GetValue(CheckedForegroundProperty); }
            set { SetValue(CheckedForegroundProperty, value); }
        }

        public static readonly DependencyProperty CheckedBackgroundProperty =
            DependencyProperty.Register("CheckedBackground", typeof(Brush), typeof(SlideCheckBox), new PropertyMetadata(Brushes.LimeGreen));
        /// <summary>
        /// 选中状态背景色
        /// </summary>
        public Brush CheckedBackground
        {
            get { return (Brush)GetValue(CheckedBackgroundProperty); }
            set { SetValue(CheckedBackgroundProperty, value); }
        }


        /// <summary>
        /// 设置圆角
        /// </summary>
        public CornerRadius  CornerRadius
        {
            get { return (CornerRadius )GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CornerRadius.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CornerRadiusProperty =
            DependencyProperty.Register("CornerRadius", typeof(CornerRadius ), typeof(SlideCheckBox), new PropertyMetadata(new CornerRadius(0)));



        /// <summary>
        /// 选中状态下的Padding
        /// </summary>
        public Thickness  CheckedPadding
        {
            get { return (Thickness )GetValue(CheckedPaddingProperty); }
            set { SetValue(CheckedPaddingProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CheckedPadding.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CheckedPaddingProperty =
            DependencyProperty.Register("CheckedPadding", typeof(Thickness ), typeof(SlideCheckBox), new PropertyMetadata(new Thickness(0)));

        
    }
}
