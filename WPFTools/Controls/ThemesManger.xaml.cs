﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WPFTools.Presentation;

namespace WPFTools.Controls
{
    /// <summary>
    /// ThemesManger.xaml 的交互逻辑
    /// </summary>
    public partial class ThemesManger : UserControl
    {
        public ThemesManger()
        {
            InitializeComponent();
            this.DataContext = this;
            SelectedAccentColor = _AccentColors[4];
        }

       
        private Color[] _AccentColors = new Color[]{
            Color.FromRgb(0xa4, 0xc4, 0x00),   // lime
            Color.FromRgb(0x60, 0xa9, 0x17),   // green
            Color.FromRgb(0x00, 0x8a, 0x00),   // emerald
            Color.FromRgb(0x00, 0xab, 0xa9),   // teal
            Color.FromRgb(0x1b, 0xa1, 0xe2),   // cyan
            Color.FromRgb(0x00, 0x50, 0xef),   // cobalt
            Color.FromRgb(0x6a, 0x00, 0xff),   // indigo
            Color.FromRgb(0xaa, 0x00, 0xff),   // violet
            Color.FromRgb(0xf4, 0x72, 0xd0),   // pink
            Color.FromRgb(0xd8, 0x00, 0x73),   // magenta
            Color.FromRgb(0xa2, 0x00, 0x25),   // crimson
            Color.FromRgb(0xe5, 0x14, 0x00),   // red
            Color.FromRgb(0xfa, 0x68, 0x00),   // orange
            Color.FromRgb(0xf0, 0xa3, 0x0a),   // amber
            Color.FromRgb(0xe3, 0xc8, 0x00),   // yellow
            Color.FromRgb(0x82, 0x5a, 0x2c),   // brown
            Color.FromRgb(0x6d, 0x87, 0x64),   // olive
            Color.FromRgb(0x64, 0x76, 0x87),   // steel
            Color.FromRgb(0x76, 0x60, 0x8a),   // mauve
            Color.FromRgb(0x87, 0x79, 0x4e),   // taupe
        };



        public Color[] AccentColors {
            get
            {
                return _AccentColors;
            }
        }


        /// <summary>
        /// 设置选择的颜色
        /// </summary>
        public Color SelectedAccentColor
        {
            get { return (Color)GetValue(SelectedAccentColorProperty); }
            set
            {
                SetValue(SelectedAccentColorProperty, value);
            }
        }
       private static void onSelectedAccentColorChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.Property == SelectedAccentColorProperty)
            {
                AppearanceManager.Current.AccentColor = (Color)e.NewValue;
            }
        }

        // Using a DependencyProperty as the backing store for SelectedAccentColor.  This enables animation, styling, binding, etc...
       public static DependencyProperty SelectedAccentColorProperty = DependencyProperty.Register("SelectedAccentColor", typeof(Color), typeof(ThemesManger), new PropertyMetadata(AppearanceManager.Current.AccentColor, new PropertyChangedCallback(onSelectedAccentColorChange)));



        public int SelectedTheme
        {
            get { return (int)GetValue(SelectedThemeProperty); }
            set { SetValue(SelectedThemeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedTheme.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedThemeProperty =
            DependencyProperty.Register("SelectedTheme", typeof(int), typeof(ThemesManger), new PropertyMetadata(0,new PropertyChangedCallback(onSelectedThemeChange)));
         private static void onSelectedThemeChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.Property == SelectedThemeProperty)
            {
               int chose =(int) e.NewValue ; //选择的值
               if (chose == 0)
               {
                   AppearanceManager.Current.ThemeSource = AppearanceManager.LightThemeSource;
               }
               else if(chose ==1) {
                   AppearanceManager.Current.ThemeSource = AppearanceManager.DarkThemeSource;
               }
            }
        }



         public double SelectedFontSize
         {
             get { return (double)GetValue(SelectedFontSizeProperty); }
             set { SetValue(SelectedFontSizeProperty, value); }
         }

         // Using a DependencyProperty as the backing store for SelectedFontSize.  This enables animation, styling, binding, etc...
         public static readonly DependencyProperty SelectedFontSizeProperty =
             DependencyProperty.Register("SelectedFontSize", typeof(double), typeof(ThemesManger), new PropertyMetadata(AppearanceManager.Current.FontSize, new PropertyChangedCallback(onSelectedFontSizeChange)));


         private static void onSelectedFontSizeChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
         {
             if (e.Property == SelectedFontSizeProperty)
             {
                 double chose = (double)e.NewValue; //选择的值
                 if (chose>0)
                 {
                   AppearanceManager.Current.FontSize = chose;
                 }
             }
         }

         private double[] _fontSizes = { 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 };
        //字体大小选择项
         public double[] FontSizes
         {
             get { return _fontSizes; }
             set { _fontSizes = value; }
         }
        

        
    }
}
