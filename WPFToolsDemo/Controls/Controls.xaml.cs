﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WPFToolsDemo
{
     
    /// <summary>
    /// Controls.xaml 的交互逻辑
    /// </summary>
    public partial class Controls : Window
    {

        public Controls()
        {
            InitializeComponent();
            this.DataContext = this;
            buttonCommand = new ButtonCommand();
        }
        public ICommand buttonCommand { get; set; }


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //String msg = port.Port + "," + port.BaudRate.ToString() + "," + port.Parity.ToString() + "," +port.DataBits+","+ port.StopBits;
            //MessageBox.Show(msg);
        }

        public class ButtonCommand : ICommand
        {
            public bool CanExecute(object parameter)
            {
                return true;
            }


            public void Execute(object parameter)
            {
                MessageBox.Show("点击");
            }


            public event EventHandler CanExecuteChanged;
        }



    }
}
