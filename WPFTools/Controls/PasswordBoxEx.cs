﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFTools.Controls
{
    /// <summary>
    ///  自定义密码框
    /// </summary>
    [TemplatePart(Name = "PART_Password", Type = typeof(PasswordBox))]
    [TemplatePart(Name = "PART_Button", Type = typeof(Button))]
    public class PasswordBoxEx : Control
    {
        private const string PART_Button = "PART_Button"; //主要部分
        private const String PART_Password = "PART_Password";
        private PasswordBox pwdBox; //私有Parity密码框的对象
        private Button button;
        static PasswordBoxEx()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(PasswordBoxEx), new FrameworkPropertyMetadata(typeof(PasswordBoxEx)));
        }

      
        /// <summary>
        /// 模板加载时
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            pwdBox = GetTemplateChild(PART_Password) as PasswordBox;

            pwdBox.PasswordChanged += pwdBox_PasswordChanged; //添加密码更改的事件
            pwdBox.LostFocus += PwdBox_LostFocus;

            button = GetTemplateChild(PART_Button) as Button;
            button.Click += Button_Click;
         }
        /// <summary>
        /// 删除按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            pwdBox.Clear();
        }

        private void PwdBox_LostFocus(object sender, RoutedEventArgs e)
        {
            IsPasswordBoxFocus = false;
        }

        /// <summary>
        /// 获取焦点时 将焦点转移到输入框去
        /// </summary>
        /// <param name="e"></param>
        protected override void OnGotFocus(RoutedEventArgs e)
        {
            pwdBox.Focus();
            IsPasswordBoxFocus = true;
            
           //base.OnGotFocus(e);
        }

       
        /// <summary>
        /// 密码修改事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void pwdBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            //更改密码
            SetValue(PasswordProperty,pwdBox.Password);
          
        }

        public string Password
        {
            get { return (string)GetValue(PasswordProperty); }
            set
            {
                
                SetValue(PasswordProperty, value);
                //设置密码框密码
                pwdBox.Password = value;
                
            }
        }

        /// <summary>
        /// 输入的密码
        /// </summary>
        public static readonly DependencyProperty PasswordProperty =
            DependencyProperty.Register("Password", typeof(string), typeof(PasswordBoxEx), new PropertyMetadata(""));
       
        

        public static readonly DependencyProperty CornerRadiusProperty =
               DependencyProperty.Register("CornerRadius", typeof(CornerRadius), typeof(PasswordBoxEx), new PropertyMetadata(new CornerRadius(0)));
        /// <summary>
        /// 圆角大小,左上，右上，右下，左下
        /// </summary>
        public CornerRadius CornerRadius
        {
            get { return (CornerRadius)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }



        public string WaterMark
        {
            get { return (string)GetValue(WaterMarkProperty); }
            set { SetValue(WaterMarkProperty, value); }
        }

        

        /// <summary>
        /// 水印内容 
        /// </summary>
        public static readonly DependencyProperty WaterMarkProperty =
            DependencyProperty.Register("WaterMark", typeof(string), typeof(PasswordBoxEx), new PropertyMetadata(""));


        public string Label
        {
            get { return (string)GetValue(LabelProperty); }
            set { SetValue(LabelProperty, value); }
        }

       /// <summary>
       /// Label标签的内容
       /// </summary>
        public static readonly DependencyProperty LabelProperty =
            DependencyProperty.Register("Label", typeof(string), typeof(PasswordBoxEx), new PropertyMetadata(""));

        public double LabelWidth
        {
            get { return (double)GetValue(LabelWidthProperty); }
            set { SetValue(LabelWidthProperty, value); }
        }

        /// <summary>
        /// Label标签的宽度
        /// </summary>
        public static readonly DependencyProperty LabelWidthProperty =
            DependencyProperty.Register("LabelWidth", typeof(double), typeof(PasswordBoxEx), new PropertyMetadata(double.NaN));


        /// <summary>
        ///  Label 的 Margin 属性
        /// </summary>
        public static readonly DependencyProperty LabelMarginProperty =
            DependencyProperty.Register("LabelMargin", typeof(Thickness), typeof(PasswordBoxEx), new PropertyMetadata(new Thickness(5, 2, 5, 2)));


        public HorizontalAlignment LabelHorizontalAlignment
        {
            get { return (HorizontalAlignment)GetValue(LabelHorizontalAlignmentProperty); }
            set { SetValue(LabelHorizontalAlignmentProperty, value); }
        }

        /// <summary>
        /// Label 的 HorizontalAlignment 属性
        /// </summary>
        public static readonly DependencyProperty LabelHorizontalAlignmentProperty =
            DependencyProperty.Register("LabelHorizontalAlignment", typeof(HorizontalAlignment), typeof(PasswordBoxEx), new PropertyMetadata(HorizontalAlignment.Right));


        public bool IsPasswordBoxFocus
        {
            get { return (bool)GetValue(IsPasswordBoxFocusProperty); }
            set { SetValue(IsPasswordBoxFocusProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsPasswordBoxFocus.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsPasswordBoxFocusProperty =
            DependencyProperty.Register("IsPasswordBoxFocus", typeof(bool), typeof(PasswordBoxEx), new PropertyMetadata(false));


        /// <summary>
        /// 是否为清空按钮
        /// </summary>
        public bool IsClearButton
        {
            get { return (bool)GetValue(IsClearButtonProperty); }
            set { SetValue(IsClearButtonProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsClearButton.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsClearButtonProperty =
            DependencyProperty.Register("IsClearButton", typeof(bool), typeof(PasswordBoxEx), new PropertyMetadata(false));

    }
}
