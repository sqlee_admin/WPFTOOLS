﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading;

namespace WPFTools.Dialogs
{
    /// <summary>
    /// SplashBox.xaml 的交互逻辑
    /// </summary>
    public partial class SplashBox 
    {
        public SplashBox()
        {
            InitializeComponent();
        }
        public ManualResetEvent mre1;

        public void ShowProgress(String Value)
        {
            //pgbProcess.Value = Value;
            txtInfo.Text = "已完成" + Value.ToString() + "%";
        }


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //通知主线程自己已经启动完毕
            //mre1.Set();
            this.DialogResult = true;
        }
    }
}
